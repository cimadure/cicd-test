import time
#import benchmark


import time
import unittest
import pytest


def something(duration=0.000001):
    """
    Function that needs some serious benchmarking.
    """
    time.sleep(duration)
    # You may return anything you want, like the result of a computation
    return 123


class MyProject(object):
    pass