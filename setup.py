#!/usr/bin/env python

from setuptools import setup, find_packages

def readme():
    with open('README.md') as f:
        return f.read()

setup(name='cicd-benchmark',
      version='0.1',
      description='Python Example of benchmark',
      author='Ronan Cimadure',
      author_email='ronan.cimadure@gmail.com',
      url='http://google.com',
      packages=find_packages(),
      long_description=readme()
     )


