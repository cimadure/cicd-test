[![pipeline status](https://gitlab.com/cimadure/cicd-test/badges/master/pipeline.svg)](https://gitlab.com/cimadure/cicd-test/commits/master)
[![coverage report](https://gitlab.com/cimadure/cicd-test/badges/master/coverage.svg)](https://gitlab.com/cimadure/cicd-test/commits/master)

READ me PLEASE


pytest

pytest --benchmark-histogram

pytest --cov=myproject tests


# TEST with coverage
`python -m pytest --cov=myproject tests/`

(works because Python adds the current directory in the PYTHONPATH for you.)